import pygtk
pygtk.require("2.0")

import gtk
from gtk import glade
from gtk import gdk

from xournal import journal

predefined_colors = { 'black': '#000000'
                    , 'blue': '#3333cc'
                    , 'red': '#ff0000'
                    , 'green': '#008000'
                    , 'grey': '#808080'
                    , 'light_blue': '#00c0ff'
                    , 'light_green': '#00ff00'
                    , 'magenta': '#ff00ff'
                    , 'orange': '#ff8000'
                    , 'yellow': '#ffff00'
                    , 'white': '#ffffff'
                    }

class Interface(object):
    def __init__(self):
        self.glade_tree = glade.XML('xournal.glade', root='winMain')
        s = self.glade_tree.get_widget('scrolledwindowMain')
        #s = gtk.ScrolledWindow()
        s.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        self.journal = journal.Journal()
        self.journal.set_size_request(800, 600)
        self.journal.set_scroll_region(0,-1,800,600)
        s.add(self.journal)
        self.journal.show_all()
        self.signals = { 'on_fileQuit_activate': self.quit
                       , 'on_winMain_delete_event': self.quit
                       , 'on_buttonColorChooser_set': self.set_color('custom')
                       , 'on_colorWhite_activate': self.set_color('white')
                       , 'on_colorBlack_activate': self.set_color('black')
                       , 'on_colorBlue_activate': self.set_color('blue')
                       , 'on_colorRed_activate': self.set_color('red')
                       , 'on_colorGreen_activate': self.set_color('green')
                       , 'on_colorGray_activate': self.set_color('grey')
                       , 'on_colorLightBlue_activate': self.set_color('light_blue')
                       , 'on_colorLightGreen_activate': self.set_color('light_green')
                       , 'on_colorMagenta_activate': self.set_color('magenta')
                       , 'on_colorOrange_activate': self.set_color('orange')
                       , 'on_colorYellow_activate': self.set_color('yellow')
                       , 'on_buttonFine_clicked': self.set_width(2)
                       , 'on_buttonMedium_clicked': self.set_width(3)
                       , 'on_buttonThick_clicked': self.set_width(4)
                       , 'on_penthicknessVeryFine_activate': self.set_width(1)
                       , 'on_penthicknessFine_activate': self.set_width(2)
                       , 'on_penthicknessMedium_activate': self.set_width(3)
                       , 'on_penthicknessThick_activate': self.set_width(4)
                       , 'on_penthicknessVeryThick_activate': self.set_width(5)
                       }
        self.glade_tree.signal_autoconnect(self.signals)
        self.glade_tree.get_widget('winMain').set_icon_from_file('pixmaps/xournal.png')

    def quit(self, widget, event=None):
        gtk.mainquit()

    def set_color(self, color):
        def custom_color(widget):
            self.journal.set_color(widget.get_color())

        def predefined_color(widget):
            try:
                self.set_color_from_string(predefined_colors[color])
            except KeyError:
                # The color isn't predefined?
                self.set_color_from_string(color)

        if color is 'custom':
            return custom_color
        else:
            return predefined_color

    def set_color_from_string(self, string):
        self.journal.set_color(gdk.color_parse(string))

    def set_width(self, width, callback=True):
        if callback:
            def set_width_callback(widget):
                return self.journal.set_width(width)
            return set_width_callback
        else:
            self.journal.set_width(width)
