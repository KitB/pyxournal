import gtk
from gtk import gdk
import gnomecanvas

class Journal(gnomecanvas.Canvas):
    def __init__(self):
        super(Journal, self).__init__(aa=True)
        #self = gnomecanvas.Canvas()
        self.set_events( gdk.EXPOSURE_MASK
                       | gdk.POINTER_MOTION_MASK
                       | gdk.BUTTON_MOTION_MASK
                       | gdk.BUTTON_PRESS_MASK
                       | gdk.BUTTON_RELEASE_MASK
                       | gdk.KEY_PRESS_MASK
                       | gdk.ENTER_NOTIFY_MASK
                       | gdk.LEAVE_NOTIFY_MASK
                       )
        self.set_center_scroll_region(True)
        self.background = Background(border="blue")
        self.background.draw(self)
        self.connect('show', Journal.set_pen_cursor)
        self.connect('button_press_event', Journal.button_press)
        self.connect('button_release_event', Journal.button_release)
        self.connect('motion_notify_event', Journal.motion_notify)
        self.mode = None
        self.fg_color = gdk.color_parse('#A6E22E')
        self.pen_width = 2

        for device in gdk.devices_list():
            if device is not gdk.device_get_core_pointer() and device.num_axes >= 2:
                device.set_mode(gdk.MODE_SCREEN)
        self.set_extension_events(gdk.EXTENSION_EVENTS_CURSOR)

    def set_width(self, width):
        self.pen_width = width

    def set_color(self, color):
        self.fg_color = color

    def get_pointer_coords(self, event):
        return self.window_to_world(event.x, event.y)

    def render_path(self):
        path_def = gnomecanvas.path_def_new(self.cur_path)
        self.current_item.set_bpath(path_def)

    def button_press(self, event):
        is_core = event.device is gdk.device_get_core_pointer()
        if event.button is 1:
            # Left click
            self.mode = 'drawing'
            (x, y) = self.get_pointer_coords(event)
            self.cur_path = [(gnomecanvas.MOVETO_OPEN, x, y)]
            self.current_item = self.root().add( gnomecanvas.CanvasBpath
                                               , outline_color=self.fg_color
                                               , width_pixels=self.pen_width
                                               , cap_style=gdk.CAP_ROUND
                                               )
        elif event.button is 2:
            # Middle click
            self.mode = 'erasing'

        elif event.button is 3:
            # Right click
            self.mode = 'erasing'

    def button_release(self, event):
        self.mode = None

    def motion_notify(self, event):
        is_core = event.device is gdk.device_get_core_pointer()
        pressure = event.get_axis(gdk.AXIS_PRESSURE) or 0
        self.set_pen_cursor(pressure)
        if not is_core and self.mode is 'drawing':
            #print "(%03.2f, %03.2f)\t%03.2f" % (event.x, event.y, pressure)
            (x, y) = self.get_pointer_coords(event)
            self.cur_path.append((gnomecanvas.LINETO, x, y))
            self.render_path()
        elif self.mode is 'erasing':
            (x, y) = self.get_pointer_coords(event)
            under = self.get_item_at(x, y)
            if isinstance(under, gnomecanvas.CanvasBpath):
                #under = gnomecanvas.CanvasBpath()
                under.destroy()

    def set_pen_cursor(canvas, pressure=0):
        sides = int(10*(pressure+0.5))
        pm = gdk.Pixmap(None, sides, sides, 1)
        mask = gdk.Pixmap(None, sides, sides, 1)
        colormap = gdk.colormap_get_system()
        black = colormap.alloc_color('black')
        white = colormap.alloc_color('white')
        bgc = pm.new_gc(foreground=black)
        wgc = pm.new_gc(foreground=white)
        mask.draw_rectangle(bgc, True, 0,0, sides, sides)
        pm.draw_rectangle(bgc, True, 0,0, sides, sides)

        pm.draw_arc(wgc, True, 0, 0, sides, sides, 0, 360*64)
        mask.draw_arc(wgc, True, 0, 0, sides, sides, 0, 360*64)

        c = int(sides/2)
        cur = gdk.Cursor(pm, mask, canvas.fg_color, gdk.color_parse('white'), c, c)
        canvas.window.set_cursor(cur)

class Page(object):
    def __init__(self, journal, background=None, size=(800, 600)):
        self.journal = journal
        self.background = background or Background()
        self.background.draw(self)

class Background(object):
    def __init__(self, color="#272822", border=None):
        self.color = color
        self.border = border or color
    def draw(self, canvas, y=0):
        self.canvas_item = canvas.root().add( gnomecanvas.CanvasRect
                                            , outline_color=self.border, fill_color=self.color
                                            , x1=0.0, y1=y
                                            , x2=792.0, y2=612.0
                                            )
